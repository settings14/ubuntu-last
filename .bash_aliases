PS1="\u:\W\$ "

alias cp="cp -rv"
alias rm="rm -rv"
alias mkd="mkdir -p"
alias mv="mv -i"
alias rmgr="ls | grep -e "GOT.\[S05E0[1-8]\]" | xargs rm"

alias osup="sudo apt update && sudo apt upgrade -y && sudo apt autoremove -y && sudo snap refresh"
alias eb="vim $HOME/.bash_aliases"
alias sos="source $HOME/.bashrc"
alias renow="sudo shutdown -r now"

alias cdl="cd $HOME/Downloads"
alias cdh="cd $HOME/Downloads/qbit/shows"
alias cdd="cd $HOME/dev"
alias cdc="cd $HOME/dev/ass/crewman"

alias arty="php artisan"

function mkcd {
  last=$(eval "echo \$$#")
  if [ ! -n "$last" ]; then
    echo "Enter a directory name"
  elif [ -d $last ]; then
    echo "\`$last' already exists"
  else
    mkdir $@ -p  && cd $last
  fi
}
